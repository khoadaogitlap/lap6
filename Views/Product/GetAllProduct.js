﻿class ProductRepository {
    async getProducts() {
        try {
            const response = await fetch('/api/products');
            const data = await response.json();
            return data;
        } catch (error) {
            console.error(error);
            throw new Error('Failed to fetch products.');
        }
    }

    async addProduct(product) {
        try {
            const response = await fetch('/api/products', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(product)
            });
            const data = await response.json();
            return data;
        } catch (error) {
            console.error(error);
            throw new Error('Failed to add product.');
        }
    }

    // Add other methods for update, delete, etc.
}